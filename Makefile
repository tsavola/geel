# Copyright 2006  Timo Savola
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

-include BuildOptions.mk
include $(BUILD)/build.mk
include $(if $(COMMON),$(COMMON)/module.mk)
include $(if $(SPACE),$(SPACE)/module.mk)

geel_NAME		= geel
geel_VERSION		= 0
geel_SOURCES		= geel/gl.cpp geel/proc-glx.cpp
geel_LIBS		= -lGL

$(call library,geel)

geel_context_NAME	= geel-context
geel_context_VERSION	= 0
geel_context_SOURCES	= geel/context.cpp geel/window.cpp geel/xembed.cpp
geel_context_LIBS	= -lGL -lX11 -lpthread

$(call library,geel_context)

test_NAME		= test
test_SOURCES		= test/test.cpp
test_LIBS		= -lgeel -lgeel-context -lGL -lX11 -lpthread
test_DEPENDS		= geel geel_context

$(call binary,test)

TAGS_SOURCES		= $(wildcard geel/*.hpp)

build: all-optimize all-debug-static

install: build
	install -d $(DESTDIR)$(PREFIX)/include/geel
	install -d $(DESTDIR)$(PREFIX)/lib

	install -m 644 geel/*.[ht]pp $(DESTDIR)$(PREFIX)/include/geel/

	install -m 644 $(geel_STATIC_LIBRARY_optimize) \
	               $(DESTDIR)$(PREFIX)/lib/
	install -m 644 $(geel_SHARED_LIBRARY_optimize) \
	               $(DESTDIR)$(PREFIX)/lib/
	ln -sf $(geel_SHARED_LIBRARY_NAME) \
	       $(DESTDIR)$(PREFIX)/lib/$(geel_SHARED_GENERIC_NAME)
	install -m 644 $(geel_STATIC_LIBRARY_debug) \
	               $(DESTDIR)$(PREFIX)/lib/lib$(geel_NAME)-dbg.a

	install -m 644 $(geel_context_STATIC_LIBRARY_optimize) \
	               $(DESTDIR)$(PREFIX)/lib/
	install -m 644 $(geel_context_SHARED_LIBRARY_optimize) \
	               $(DESTDIR)$(PREFIX)/lib/
	ln -sf $(geel_context_SHARED_LIBRARY_NAME) \
	       $(DESTDIR)$(PREFIX)/lib/$(geel_context_SHARED_GENERIC_NAME)
	install -m 644 $(geel_context_SHARED_LIBRARY_debug) \
	               $(DESTDIR)$(PREFIX)/lib/lib$(geel_context_NAME)-dbg.a
