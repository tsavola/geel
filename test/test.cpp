/*
 * Copyright 2006  Timo Savola
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 */

#include <geel/gl.hpp>
#include <geel/context.hpp>
#include <geel/window.hpp>
#include <space/angle.hpp>
#include <boost/cstdint.hpp>
#include <iostream>
#include <cstdlib>
#include <cstddef>

using std::size_t;
using boost::uint16_t;
using namespace space;

namespace {

const char * const vertex_source =
	" attrib vec3 vertex; "
	" void main() { gl_Position = vertex; } ";

double aspect = 0;

void resize(uint16_t width, uint16_t height)
{
	aspect = double(width) / double(height);
}

void init(uint16_t width, uint16_t height)
{
	resize(width, height);

#if 1
	vec<3> vertex_data[16];

	vertex_data[ 0] = vec<3>(-1, -1,  1);
	vertex_data[ 1] = vec<3>(-1,  1,  1);
	vertex_data[ 2] = vec<3>( 1,  1,  1);
	vertex_data[ 3] = vec<3>( 1, -1,  1);
	vertex_data[ 4] = vec<3>( 1, -1, -1);
	vertex_data[ 5] = vec<3>(-1, -1, -1);
	vertex_data[ 6] = vec<3>(-1,  1, -1);
	vertex_data[ 7] = vec<3>(-1,  1,  1);

	vertex_data[ 8] = vec<3>( 1,  1, -1);
	vertex_data[ 9] = vec<3>( 1,  1,  1);
	vertex_data[10] = vec<3>(-1,  1,  1);
	vertex_data[11] = vec<3>(-1,  1, -1);
	vertex_data[12] = vec<3>(-1, -1, -1);
	vertex_data[13] = vec<3>( 1, -1, -1);
	vertex_data[14] = vec<3>( 1, -1,  1);
	vertex_data[15] = vec<3>( 1,  1,  1);

	gl::buffer vertex_buffer(gl::array);
	vertex_buffer.bind();
	vertex_buffer.data(sizeof (vertex_data), vertex_data, gl::static_draw);

	gl::shader vertex_shader(gl::vertex_shader);
	vertex_shader.source(1, &vertex_source, 0);
	BOOST_ASSERT(vertex_shader.compile());

	gl::program program;
	program.attach(vertex_shader);
	BOOST_ASSERT(program.link());
	program.use();

	gl::attrib vertex_attrib = program.attrib("vertex");
	vertex_attrib.pointer(16, gl::float_type, false, 0, 0);
	vertex_attrib.enable();

	gl::matrix_mode(gl::projection);

	gl::load_identity();
	gl::frustum(-0.1 * aspect, 0.1 * aspect, 0.1, -0.1, 0.2, 100);

	gl::matrix_mode(gl::modelview);

	gl::enable(gl::depth_test);
	gl::enable(gl::cull_face);
#endif
}

void draw(angle<degree> a)
{
	float c = a.degrees() / 360;
	gl::clear_color(c, c, c, 1);
	gl::clear(gl::color_buffer_bit);

#if 1
	gl::clear(gl::color_buffer_bit | gl::depth_buffer_bit);

	gl::load_identity();
	gl::translate(0.0, 0.0, -4.0);
	gl::rotate(a, vec<3>(0, 1, 0));

	gl::draw_arrays(gl::triangle_fan, 0, 8);
	gl::draw_arrays(gl::triangle_fan, 8, 8);
#endif
}

void print(const std::vector<std::string> & set, const char * message)
{
	if (set.empty())
		return;

	std::cerr << message << std::endl;

	std::vector<std::string>::const_iterator i;
	for (i = set.begin(); i != set.end(); ++i)
		std::cerr << "\t" << *i << std::endl;
}

} // anonymous

int main()
{
	const uint16_t default_width = 640;
	const uint16_t default_height = 480;

	struct {
		gl::window * window;
		const space::angle<space::degree> angle;
	} windows[] = {
		{ new gl::window(default_width, default_height, "Geel 1"),
		  -45 },
		{ new gl::window(default_width, default_height, "Geel 2"),
		   45 }
	};

	const size_t window_count = sizeof (windows) / sizeof (windows[0]);

	try {
		windows[0].window->make_current();
		gl::init();

		for (size_t i = 0; i < window_count; i++) {
			windows[i].window->make_current();
			init(default_width, default_height);
		}

		size_t closed_count = 0;

		do {
			for (size_t i = 0; i < window_count; i++) {
				gl::window * window = windows[i].window;
				if (!window)
					continue;

				uint16_t width = 0;
				uint16_t height = 0;

				if (window->handle_events(width, height)) {
					window->make_current();

					if (width && height) {
						gl::viewport(0, 0,
							     width, height);
						resize(width, height);
					}

					draw(windows[i].angle);

					window->swap_buffers();
				} else {
					delete window;
					windows[i].window = 0;

					closed_count++;
				}
			}
		} while (closed_count < window_count);

		return EXIT_SUCCESS;
	}
	catch (const gl::extension_error & e) {
		print(e.extensions, "Missing OpenGL extensions:");
		print(e.functions, "Missing OpenGL functions:");
	}
	catch (const gl::call_error & e) {
		std::cerr << "Call error: " << e.what() << std::endl;
	}
	catch (const gl::context_error & e) {
		std::cerr << "Context error: " << e.what() << std::endl;
	}

	return EXIT_FAILURE;
}
