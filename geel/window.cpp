/*
 * Copyright 2004-2006  Timo Savola
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 */

#include "window.hpp"
#include "xembed.hpp"
#include <common/visibility.hpp>
#include <boost/assert.hpp>
#include <pthread.h>
#include <iostream>

namespace {

	struct COMMON_VISIBILITY_HIDDEN x_visual_info
	{
		x_visual_info(XVisualInfo *);
		~x_visual_info();
		XVisualInfo * operator*() const;

	private:
		XVisualInfo * const Pointer;
	};

	x_visual_info::x_visual_info(XVisualInfo * ptr) :
		Pointer(ptr)
	{
	}

	x_visual_info::~x_visual_info()
	{
		XFree(Pointer);
	}

	XVisualInfo * x_visual_info::operator*() const
	{
		return Pointer;
	}

	static pthread_mutex_t display_mutex = PTHREAD_MUTEX_INITIALIZER;
	static Display * display_pointer = 0;
	static unsigned int display_usage = 0;

}

namespace gl {

	window::x_display::x_display()
		throw (context_error)
	{
		pthread_mutex_lock(&display_mutex);

		if (!display_pointer)
			display_pointer = XOpenDisplay(0);

		bool ok = display_pointer != 0;

		if (ok)
			display_usage++;

		pthread_mutex_unlock(&display_mutex);

		if (!ok)
			throw context_error("Failed to open display");
	}

	window::x_display::~x_display()
		throw ()
	{
		pthread_mutex_lock(&display_mutex);

		if (--display_usage == 0) {
			XCloseDisplay(display_pointer);
			display_pointer = 0;
		}

		pthread_mutex_unlock(&display_mutex);
	}

	Display * window::x_display::operator*() const
	{
		return display_pointer;
	}

	window::window(parent_id parent)
		throw (context_error) :
		Delete_atom(None)
	{
		XWindowAttributes attrs;
		if (!XGetWindowAttributes(*Display_ptr, Window(parent), &attrs))
			throw context_error("Failed to get parent window attributes");

		Width = attrs.width;
		Height = attrs.height;

		int screen = XScreenNumberOfScreen(attrs.screen);

		init(screen, Window(parent));

		set_xembed_info(*Display_ptr, Window_id, xembed_mapped);
	}

	window::window(uint16_t width, uint16_t height, const char * title)
		throw (context_error) :
		Width(width),
		Height(height)
	{
		int screen = DefaultScreen(*Display_ptr);
		Window parent = RootWindow(*Display_ptr, screen);

		init(screen, parent);

		Delete_atom = XInternAtom(*Display_ptr, "WM_DELETE_WINDOW", False);
		if (Delete_atom == None)
			throw context_error("Failed to intern WM_DELETE_WINDOW atom");

		if (!XSetWMProtocols(*Display_ptr, Window_id, &Delete_atom, 1))
			throw context_error("Failed to set WM protocols");

		XTextProperty property;
		if (!XStringListToTextProperty(const_cast<char **> (&title), 1, &property))
			throw context_error("Failed to allocate text property");

		XSetWMName(*Display_ptr, Window_id, &property);
		XFree(property.value);

		XMapWindow(*Display_ptr, Window_id);
	}

	void window::init(int screen, Window parent)
		throw (context_error)
	{
		int glx_attrs[] = {
			GLX_RGBA,
			GLX_RED_SIZE,     8,
			GLX_GREEN_SIZE,   8,
			GLX_BLUE_SIZE,    8,
			GLX_DOUBLEBUFFER,
			GLX_DEPTH_SIZE,   24,
			None
		};

		x_visual_info info = glXChooseVisual(*Display_ptr, screen, glx_attrs);
		if (!*info)
			throw context_error("No suitable visuals available");

		Colormap colormap = XCreateColormap(*Display_ptr, parent, (*info)->visual, AllocNone);
		if (colormap == None)
			throw context_error("Failed to create colormap");

		XSetWindowAttributes x_attrs;
		x_attrs.background_pixel = 0;
		x_attrs.border_pixel = 0;
		x_attrs.colormap = colormap;
		x_attrs.event_mask = StructureNotifyMask | ExposureMask;

		Window_id = XCreateWindow(*Display_ptr, parent, 0, 0, Width, Height, 0,
		                          (*info)->depth, InputOutput, (*info)->visual,
		                          CWBackPixel | CWBorderPixel | CWColormap | CWEventMask,
		                          &x_attrs);
		if (Window_id == None)
			throw context_error("Failed to create window");

		Context_id = glXCreateContext(*Display_ptr, *info, 0, True);
		if (Context_id == None)
			throw context_error("Failed to create GL context");

		if (!glXIsDirect(*Display_ptr, Context_id))
			std::cerr << "GL context is !direct" << std::endl;
	}

	void window::make_current()
		throw (context_error)
	{
		if (!glXMakeCurrent(*Display_ptr, Window_id, Context_id))
			throw context_error("Failed to make GL context current");
	}

	void window::swap_buffers() const
		throw ()
	{
		glXSwapBuffers(*Display_ptr, Window_id);
	}

	COMMON_CHECK_RETURN(bool) window::handle_events(uint16_t & new_width, uint16_t & new_height)
	{
		bool proceed = true;

		while (XPending(*Display_ptr) > 0) {
			XEvent event;
			XNextEvent(*Display_ptr, &event);

			switch (event.type) {
			case ConfigureNotify:
				if (event.xconfigure.width != Width || event.xconfigure.height != Height) {
					new_width = Width = event.xconfigure.width;
					new_height = Height = event.xconfigure.height;
				}
				break;

			case ReparentNotify:
				if (is_embedded()) {
					XWindowAttributes attrs;
					if (!XGetWindowAttributes(*Display_ptr, Window_id, &attrs))
						throw context_error("Failed to get window attributes");

					if (event.xreparent.parent == attrs.root)
						proceed = false;
				}
				break;

			case DestroyNotify:
				proceed = false;
				break;

			case ClientMessage:
				if (!is_embedded()) {
					Atom atom = event.xclient.data.l[0];
					if (atom == Delete_atom)
						proceed = false;
				}
				break;
			}
		}

		return proceed;
	}

	int window::connection() const
		throw ()
	{
		return ConnectionNumber(*Display_ptr);
	}

	bool window::is_embedded() const
		throw ()
	{
		return Delete_atom == None;
	}

}
