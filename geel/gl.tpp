/*
 * Copyright 2004-2006  Timo Savola
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 */

/**
 * \file geel/gl.tpp
 * OpenGL interface - template implementation
 */

namespace gl {

	inline void scissor(const vec<2,uint16_t> & size)
	{
		scissor(0, 0, size.x, size.y);
	}

	inline void scissor(const vec<2,int16_t> & origin, const vec<2,uint16_t> & size)
	{
		scissor(origin.x, origin.y, size.x, size.y);
	}

	/**
	 * Set the color to be used by clear().
	 * \param color RGBA color value
	 */
	template <typename T>
	inline void clear_color(const vec<4,T> & color)
	{
		clear_color(color.x, color.y, color.z, color.w);
	}

	/**
	 * Set the color and alpha channel to be used by clear().
	 * \param color RGB color value
	 * \param alpha translucency value
	 */
	template <typename T>
	inline void clear_color(const vec<3,T> & color, float alpha)
	{
		clear_color(color.x, color.y, color.z, alpha);
	}

	/**
	 * Multiply the active matrix with a rotation matrix.
	 * \param degrees amount of rotation
	 * \param axis    axis around which to rotate
	 */
	template <typename T>
	inline void rotate(const T & degrees, const vec<3,T> & axis)
	{
		rotate(degrees, axis.x, axis.y, axis.z);
	}

	/**
	 * Multiply the active matrix with a rotation matrix.
	 * \param angle amount of rotation
	 * \param axis  axis around which to rotate
	 */
	template <typename Unit, typename T>
	inline void rotate(const angle<Unit,T> & angle, const vec<3,T> & axis)
	{
		rotate(angle.degrees(), axis.x, axis.y, axis.z);
	}

	/**
	 * Multiply the active matrix with a rotation matrix.
	 * \param angle amount of rotation
	 * \param x     X component of the axis
	 * \param y     Y component of the axis
	 * \param z     Z component of the axis
	 */
	template <typename Unit, typename T>
	inline void rotate(const angle<Unit,T> & angle, const T & x, const T & y, const T & z)
	{
		rotate(angle.degrees(), x, y, z);
	}

	/**
	 * Add a translation vector to the active matrix.
	 * \param offset magnitude and direction of movement
	 */
	template <typename T>
	inline void translate(const vec<3,T> & offset)
	{
		translate(offset.x, offset.y, offset.z);
	}

	/**
	 * Scale the active matrix.
	 * \param factor the scaling factors for each dimension
	 */
	template <typename T>
	inline void scale(const vec<3,T> & factor)
	{
		scale(factor.x, factor.y, factor.z);
	}

	// Attribute

	inline attrib::attrib(int index) :
		Index(index)
	{
	}

	inline attrib::attrib(const attrib & u) :
		Index(u.Index)
	{
	}

	inline attrib & attrib::operator=(int index)
	{
		Index = index;
		return *this;
	}

	inline attrib & attrib::operator=(const attrib & a)
	{
		Index = a.Index;
		return *this;
	}

	// Uniform

	inline uniform::uniform() :
		Location(-1)
	{
	}

	inline uniform::uniform(const uniform & u) :
		Location(u.Location)
	{
	}

	inline uniform & uniform::operator=(const uniform & u)
	{
		Location = u.Location;
		return *this;
	}

	/**
	 * Assigns a single vector to the uniform.
	 * \param v vector value
	 */
	template <unsigned int N, typename T>
	inline void uniform::set(const vec<N,T> & v) const
	{
		set(1, &v);
	}

	/**
	 * Assigns a single matrix to the uniform.
	 * \param m matrix value
	 */
	template <unsigned int N>
	inline void uniform::set(const mat<N,float> & m) const
	{
		set(1, &m);
	}

	/**
	 * Assigns a sequence of values to the uniform.
	 * \param s sequence of scalars, vectors or matrices
	 */
	template <typename T>
	inline void uniform::set(const std::vector<T> & s) const
	{
		set(s.size(), &s.front());
	}

}
