/*
 * Copyright 2004-2006  Timo Savola
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 */

#ifndef GEEL_WINDOW_HPP
#define GEEL_WINDOW_HPP

#include "context.hpp"
#include <common/return.hpp>
#include <boost/cstdint.hpp>
#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>
#include <X11/Xlib.h>
#include <GL/glx.h>
#include <exception>

namespace gl {

	using boost::uint16_t;
	using boost::uint32_t;

	typedef uint32_t parent_id;

	struct window : private boost::noncopyable
	{
		window(parent_id embedder) throw (context_error);
		window(uint16_t width, uint16_t height, const char * title) throw (context_error);

		void make_current() throw (context_error);
		void swap_buffers() const throw ();
		COMMON_CHECK_RETURN(bool) handle_events(uint16_t & new_width, uint16_t & new_height);
		int connection() const throw ();

	private:
		struct x_display
		{
			x_display() throw (context_error);
			~x_display() throw ();
			Display * operator*() const;
		};

		void init(int screen, Window parent) throw (context_error);
		bool is_embedded() const throw ();

		x_display Display_ptr;
		Window Window_id;
		GLXContext Context_id;
		Atom Delete_atom;
		uint16_t Width;
		uint16_t Height;
	};

}

#endif
