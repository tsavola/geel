/*
 * Copyright 2006  Timo Savola
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 */

#define GLX_GLXEXT_PROTOTYPES

#include "proc.hpp"
#include <GL/glx.h>

namespace gl {

	proc_type get_proc_address(const char * symbol)
	{
		const GLubyte * gl_symbol = reinterpret_cast<const GLubyte *> (symbol);
		return ::glXGetProcAddressARB(gl_symbol);
	}

}
