/*
 * Copyright 2006  Timo Savola
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 */

#ifndef GEEL_PROC_HPP
#define GEEL_PROC_HPP

namespace gl {

	typedef void (* proc_type)();

	proc_type get_proc_address(const char * symbol);

}

#endif
