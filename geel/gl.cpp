/*
 * Copyright 2004-2006  Timo Savola
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 */

/**
 * \file geel/gl.cpp
 * OpenGL interface - implementation
 */

#include "gl.hpp"
#include "proc.hpp"
#include <common/debug/backtrace.hpp>
#include <boost/format.hpp>
#include <boost/scoped_array.hpp>
#include <boost/assert.hpp>
#include <set>

namespace {

	static PFNGLACTIVETEXTUREPROC               glActiveTexture;
	static PFNGLCLIENTACTIVETEXTUREPROC         glClientActiveTexture;

	static PFNGLMULTIDRAWARRAYSPROC             glMultiDrawArrays;
	static PFNGLMULTIDRAWELEMENTSPROC           glMultiDrawElements;

	static PFNGLGENBUFFERSPROC                  glGenBuffers;
	static PFNGLDELETEBUFFERSPROC               glDeleteBuffers;
	static PFNGLBINDBUFFERPROC                  glBindBuffer;
	static PFNGLBUFFERDATAPROC                  glBufferData;
	static PFNGLBUFFERSUBDATAPROC               glBufferSubData;

	static PFNGLCREATESHADEROBJECTARBPROC       glCreateShader;
	static PFNGLDELETEOBJECTARBPROC             glDeleteShader;
	static PFNGLSHADERSOURCEARBPROC             glShaderSource;
	static PFNGLCOMPILESHADERARBPROC            glCompileShader;
	static PFNGLGETINFOLOGARBPROC               glGetShaderInfoLog;
	static PFNGLGETOBJECTPARAMETERIVARBPROC     glGetShaderiv;

	static PFNGLCREATEPROGRAMOBJECTARBPROC      glCreateProgram;
	static PFNGLDELETEOBJECTARBPROC             glDeleteProgram;
	static PFNGLATTACHOBJECTARBPROC             glAttachShader;
	static PFNGLDETACHOBJECTARBPROC             glDetachShader;
	static PFNGLLINKPROGRAMARBPROC              glLinkProgram;
	static PFNGLVALIDATEPROGRAMARBPROC          glValidateProgram;
	static PFNGLGETINFOLOGARBPROC               glGetProgramInfoLog;
	static PFNGLGETUNIFORMLOCATIONARBPROC       glGetUniformLocation;
	static PFNGLUSEPROGRAMOBJECTARBPROC         glUseProgram;
	static PFNGLGETOBJECTPARAMETERIVARBPROC     glGetProgramiv;

	static PFNGLUNIFORM1FARBPROC                glUniform1f;
	static PFNGLUNIFORM2FARBPROC                glUniform2f;
	static PFNGLUNIFORM3FARBPROC                glUniform3f;
	static PFNGLUNIFORM4FARBPROC                glUniform4f;
	static PFNGLUNIFORM1IARBPROC                glUniform1i;
	static PFNGLUNIFORM2IARBPROC                glUniform2i;
	static PFNGLUNIFORM3IARBPROC                glUniform3i;
	static PFNGLUNIFORM4IARBPROC                glUniform4i;
	static PFNGLUNIFORM1FVARBPROC               glUniform1fv;
	static PFNGLUNIFORM2FVARBPROC               glUniform2fv;
	static PFNGLUNIFORM3FVARBPROC               glUniform3fv;
	static PFNGLUNIFORM4FVARBPROC               glUniform4fv;
	static PFNGLUNIFORM1IVARBPROC               glUniform1iv;
	static PFNGLUNIFORM2IVARBPROC               glUniform2iv;
	static PFNGLUNIFORM3IVARBPROC               glUniform3iv;
	static PFNGLUNIFORM4IVARBPROC               glUniform4iv;
	static PFNGLUNIFORMMATRIX2FVARBPROC         glUniformMatrix2fv;
	static PFNGLUNIFORMMATRIX3FVARBPROC         glUniformMatrix3fv;
	static PFNGLUNIFORMMATRIX4FVARBPROC         glUniformMatrix4fv;

	static PFNGLBINDATTRIBLOCATIONARBPROC       glBindAttribLocation;
	static PFNGLGETATTRIBLOCATIONARBPROC        glGetAttribLocation;
	static PFNGLVERTEXATTRIBPOINTERARBPROC      glVertexAttribPointer;
	static PFNGLENABLEVERTEXATTRIBARRAYARBPROC  glEnableVertexAttribArray;
	static PFNGLDISABLEVERTEXATTRIBARRAYARBPROC glDisableVertexAttribArray;

	typedef gl::extension_error::missing_type missing_type;
	typedef std::set<std::string> avail_type;

#ifdef GEEL_CHECK
	static void check_error()
	{
		if (unsigned int code = ::glGetError())
			throw gl::call_error(gl::error_code(code));
	}
#else
# define check_error()
#endif

	static void check_ext(missing_type & missing, const avail_type & avail, const char * name, const char * name2 = 0)
	{
		if (avail.find(name) == avail.end())
			if (!name2 || avail.find(name2) == avail.end())
				missing.push_back(name);
	}

	static void init_extensions(missing_type & m)
	{
		std::set<std::string> avail;

		if (const GLubyte * str = ::glGetString(GL_EXTENSIONS))
			while (true) {
				unsigned int len = 0;
				while (str[len] != ' ' && str[len] != '\0')
					len++;

				avail.insert(std::string(reinterpret_cast<const char *> (str), len));

				if (str[len] == '\0')
					break;

				str += len + 1;
			}

		check_ext(m, avail, "GL_ARB_multitexture");
		check_ext(m, avail, "GL_EXT_multi_draw_arrays", "GL_SUN_multi_draw_arrays");
		check_ext(m, avail, "GL_ARB_vertex_buffer_object");
		check_ext(m, avail, "GL_ARB_shader_objects");
		check_ext(m, avail, "GL_ARB_vertex_shader");
		check_ext(m, avail, "GL_ARB_fragment_shader");
		check_ext(m, avail, "GL_ARB_shading_language_100");
	}

	static gl::proc_type get_proc(missing_type & missing, const char * const * names)
	{
		std::string first;

		for (unsigned int i = 0; names[i]; i++) {
			std::string sym = names[i];
			if (names[i][0] == '-') {
				sym = names[0];
				sym += &names[i][1];
			}

			if (gl::proc_type proc = gl::get_proc_address(sym.c_str()))
				return proc;

			if (i == 0)
				first = sym;
		}

		missing.push_back(first);
		return 0;
	}

	template <typename T>
	static void set_proc(missing_type & missing, T & proc, const char * name, const char * ext1 = 0, const char * ext2 = 0)
	{
		const char * const names[] = { name, ext1, ext2, 0 };
		proc = T(get_proc(missing, names));
	}

	static void init_functions(missing_type & m)
	{
		set_proc(m, glActiveTexture,            "glActiveTexture",                     "-ARB");
		set_proc(m, glClientActiveTexture,      "glClientActiveTexture",               "-ARB");

		set_proc(m, glMultiDrawArrays,          "glMultiDrawArrays",                   "-EXT", "-SUN");
		set_proc(m, glMultiDrawElements,        "glMultiDrawElements",                 "-EXT", "-SUN");

		set_proc(m, glGenBuffers,               "glGenBuffers",                        "-ARB");
		set_proc(m, glDeleteBuffers,            "glDeleteBuffers",                     "-ARB");
		set_proc(m, glBindBuffer,               "glBindBuffer",                        "-ARB");
		set_proc(m, glBufferData,               "glBufferData",                        "-ARB");
		set_proc(m, glBufferSubData,            "glBufferSubData",                     "-ARB");

		set_proc(m, glCreateShader,             "glCreateShader",                "-ObjectARB");
		set_proc(m, glDeleteShader,             "glDeleteShader",         "glDeleteObjectARB");
		set_proc(m, glShaderSource,             "glShaderSource",                      "-ARB");
		set_proc(m, glCompileShader,            "glCompileShader",                     "-ARB");
		set_proc(m, glGetShaderInfoLog,         "glGetShaderInfoLog",       "glGetInfoLogARB");
		set_proc(m, glGetShaderiv,              "glGetShaderiv",  "glGetObjectParameterivARB");

		set_proc(m, glCreateProgram,            "glCreateProgram",               "-ObjectARB");
		set_proc(m, glDeleteProgram,            "glDeleteProgram",        "glDeleteObjectARB");
		set_proc(m, glAttachShader,             "glAttachShader",         "glAttachObjectARB");
		set_proc(m, glDetachShader,             "glDetachShader",         "glDetachObjectARB");
		set_proc(m, glLinkProgram,              "glLinkProgram",                       "-ARB");
		set_proc(m, glValidateProgram,          "glValidateProgram",                   "-ARB");
		set_proc(m, glGetProgramInfoLog,        "glGetProgramInfoLog",      "glGetInfoLogARB");
		set_proc(m, glGetUniformLocation,       "glGetUniformLocation",                "-ARB");
		set_proc(m, glUseProgram,               "glUseProgram",                  "-ObjectARB");
		set_proc(m, glGetProgramiv,             "glGetProgramiv", "glGetObjectParameterivARB");

		set_proc(m, glUniform1f,                "glUniform1f",                         "-ARB");
		set_proc(m, glUniform2f,                "glUniform2f",                         "-ARB");
		set_proc(m, glUniform3f,                "glUniform3f",                         "-ARB");
		set_proc(m, glUniform4f,                "glUniform4f",                         "-ARB");
		set_proc(m, glUniform1i,                "glUniform1i",                         "-ARB");
		set_proc(m, glUniform2i,                "glUniform2i",                         "-ARB");
		set_proc(m, glUniform3i,                "glUniform3i",                         "-ARB");
		set_proc(m, glUniform4i,                "glUniform4i",                         "-ARB");
		set_proc(m, glUniform1fv,               "glUniform1fv",                        "-ARB");
		set_proc(m, glUniform2fv,               "glUniform2fv",                        "-ARB");
		set_proc(m, glUniform3fv,               "glUniform3fv",                        "-ARB");
		set_proc(m, glUniform4fv,               "glUniform4fv",                        "-ARB");
		set_proc(m, glUniform1iv,               "glUniform1iv",                        "-ARB");
		set_proc(m, glUniform2iv,               "glUniform2iv",                        "-ARB");
		set_proc(m, glUniform3iv,               "glUniform3iv",                        "-ARB");
		set_proc(m, glUniform4iv,               "glUniform4iv",                        "-ARB");
		set_proc(m, glUniformMatrix2fv,         "glUniformMatrix2fv",                  "-ARB");
		set_proc(m, glUniformMatrix3fv,         "glUniformMatrix3fv",                  "-ARB");
		set_proc(m, glUniformMatrix4fv,         "glUniformMatrix4fv",                  "-ARB");

		set_proc(m, glBindAttribLocation,       "glBindAttribLocation",                "-ARB");
		set_proc(m, glGetAttribLocation,        "glGetAttribLocation",                 "-ARB");
		set_proc(m, glVertexAttribPointer,      "glVertexAttribPointer",               "-ARB");
		set_proc(m, glEnableVertexAttribArray,  "glEnableVertexAttribArray",           "-ARB");
		set_proc(m, glDisableVertexAttribArray, "glDisableVertexAttribArray",          "-ARB");
	}

	static bool initialized = false;

}

namespace gl {

	// Data types

	data_type type<int8_t>  ::value = int8_type;
	data_type type<int16_t> ::value = int16_type;
	data_type type<int32_t> ::value = int32_type;
	data_type type<uint8_t> ::value = uint8_type;
	data_type type<uint16_t>::value = uint16_type;
	data_type type<uint32_t>::value = uint32_type;
	data_type type<float>   ::value = float_type;
	data_type type<double>  ::value = double_type;

	// Call error

	call_error::call_error(error_code c) :
		code(c)
	{
		common::get_backtrace(backtrace);
	}

	call_error::~call_error()
		throw ()
	{
	}

	const char * call_error::what() const
		throw ()
	{
		switch (code) {
		case gl::invalid_enum:
			return "Invalid enumerant";

		case gl::invalid_value:
			return "Invalid value";

		case gl::invalid_operation:
			return "Invalid operation";

		case gl::stack_overflow:
			return "Stack overflow";

		case gl::stack_underflow:
			return "Stack underflow";

		case gl::out_of_memory:
			return "Out of memory";

		case gl::table_too_large:
			return "Table too large";

		default:
			return "Unknown";
		}
	}

	// Extension error

	extension_error::~extension_error()
		throw ()
	{
	}

	const char * extension_error::what() const
		throw ()
	{
		return "Required extensions missing";
	}

	/**
	 * Verifies that required extension are present and resolves extension functions.  A GL
	 * context must be activated before calling this function.  This function can be called
	 * several times, but only the first successful call has an effect.
	 *
	 * \throw extension_error if required OpenGL extensions are not supported or if some
	 *                        functions are missing
	 */
	void init()
		throw (extension_error)
	{
		if (!initialized) {
			extension_error e;

			init_extensions(e.extensions);
			if (!e.extensions.empty())
				throw e;

			init_functions(e.functions);
			if (!e.functions.empty())
				throw e;

			initialized = true;
		}
	}

	// Flush

	void flush()
	{
		::glFlush();
		check_error();
	}

	// Viewport

	void viewport(int16_t x, int16_t y, uint16_t width, uint16_t height)
	{
		::glViewport(x, y, width, height);
		check_error();
	}

	// Scissor

	void scissor(int16_t x, int16_t y, uint16_t width, uint16_t height)
	{
		::glScissor(x, y, width, height);
		check_error();
	}

	// Capability

	void enable(capability c)
	{
		::glEnable(c);
		check_error();
	}

	void disable(capability c)
	{
		::glDisable(c);
		check_error();
	}

	// Clear

	void clear_color(float r, float g, float b, float a)
	{
		::glClearColor(r, g, b, a);
		check_error();
	}

	void clear_depth(double d)
	{
		::glClearDepth(d);
		check_error();
	}

	void clear(unsigned int bitmask)
	{
		::glClear(bitmask);
		check_error();
	}

	// Matrix

	void matrix_mode(matrix_stack mode)
	{
		::glMatrixMode(mode);
		check_error();
	}

	void push_matrix()
	{
		::glPushMatrix();
		check_error();
	}

	void pop_matrix()
	{
		::glPopMatrix();
		check_error();
	}

	void load_identity()
	{
		::glLoadIdentity();
		check_error();
	}

	void load_matrix(const mat<4,float> & m)
	{
		::glLoadMatrixf(m.data());
		check_error();
	}

	void load_matrix(const mat<4,double> & m)
	{
		::glLoadMatrixd(m.data());
		check_error();
	}

	void mult_matrix(const mat<4,float> & m)
	{
		::glMultMatrixf(m.data());
		check_error();
	}

	void mult_matrix(const mat<4,double> & m)
	{
		::glMultMatrixd(m.data());
		check_error();
	}

	// Projection

	void frustum(double left, double right, double bottom, double top, double near, double far)
	{
		::glFrustum(left, right, bottom, top, near, far);
		check_error();
	}

	void ortho(double left, double right, double bottom, double top, double near, double far)
	{
		::glOrtho(left, right, bottom, top, near, far);
		check_error();
	}

	// Transformation

	void rotate(float a, float x, float y, float z)
	{
		::glRotatef(a, x, y, z);
		check_error();
	}

	void rotate(double a, double x, double y, double z)
	{
		::glRotated(a, x, y, z);
		check_error();
	}

	void translate(float x, float y, float z)
	{
		::glTranslatef(x, y, z);
		check_error();
	}

	void translate(double x, double y, double z)
	{
		::glTranslated(x, y, z);
		check_error();
	}

	void scale(float x, float y, float z)
	{
		::glScalef(x, y, z);
		check_error();
	}

	void scale(double x, double y, double z)
	{
		::glScaled(x, y, z);
		check_error();
	}

	// Texture unit

	void active_texture(texture_unit unit)
	{
		::glActiveTexture(GL_TEXTURE0 + unit);
		check_error();
	}

	void active_texcoord(texture_unit unit)
	{
		::glClientActiveTexture(GL_TEXTURE0 + unit);
		check_error();
	}

	// Texture

	texture::texture(capability t) :
		target(t)
	{
		BOOST_ASSERT(initialized);
		::glGenTextures(1, &Name);
		check_error();
	}

	texture::~texture()
	{
		::glDeleteTextures(1, &Name);
	}

	void texture::bind() const
	{
		::glBindTexture(target, Name);
		check_error();
	}

	void texture::min_filter(filter_mode mode)
	{
		::glTexParameteri(target, GL_TEXTURE_MIN_FILTER, mode);
		check_error();
	}

	void texture::mag_filter(filter_mode mode)
	{
		::glTexParameteri(target, GL_TEXTURE_MAG_FILTER, mode);
		check_error();
	}

	void texture::border_color(const vec<4,int32_t> & color)
	{
		::glTexParameteriv(target, GL_TEXTURE_BORDER_COLOR, color.data());
		check_error();
	}

	void texture::border_color(const vec<4,float> & color)
	{
		::glTexParameterfv(target, GL_TEXTURE_BORDER_COLOR, color.data());
		check_error();
	}

	// 1D Texture

	texture_1D::texture_1D() :
		texture(texturing_1D)
	{
	}

	void texture_1D::wrap(wrap_mode mode)
	{
		::glTexParameteri(target, GL_TEXTURE_WRAP_S, mode);
		check_error();
	}

	void texture_1D::image(unsigned int level, format int_format, uint16_t width, uint16_t border, format buf_format, void * buf)
	{
		::glTexImage1D(target, level, int_format, width, border, buf_format, GL_UNSIGNED_BYTE, buf);
		check_error();
	}

	void texture_1D::subimage(unsigned int level, int16_t xoffset, uint16_t width, format buf_format, void * buf)
	{
		::glTexSubImage1D(target, level, xoffset, width, buf_format, GL_UNSIGNED_BYTE, buf);
		check_error();
	}

	void texture_1D::copy_image(unsigned int level, format int_format, int16_t x, int16_t y, uint16_t width, uint16_t border)
	{
		::glCopyTexImage1D(target, level, int_format, x, y, width, border);
		check_error();
	}

	void texture_1D::copy_subimage(unsigned int level, int16_t xoffset, int16_t x, int16_t y, uint16_t width)
	{
		::glCopyTexSubImage1D(target, level, xoffset, x, y, width);
		check_error();
	}

	// 2D Texture

	texture_2D::texture_2D() :
		texture(texturing_2D)
	{
	}


	void texture_2D::wrap_s(wrap_mode mode)
	{
		::glTexParameteri(target, GL_TEXTURE_WRAP_S, mode);
		check_error();
	}

	void texture_2D::wrap_t(wrap_mode mode)
	{
		::glTexParameteri(target, GL_TEXTURE_WRAP_T, mode);
		check_error();
	}

	void texture_2D::image(unsigned int level, format int_format, uint16_t width, uint16_t height, uint16_t border, format buf_format, void * buf)
	{
		::glTexImage2D(target, level, int_format, width, height, border, buf_format, GL_UNSIGNED_BYTE, buf);
		check_error();
	}

	void texture_2D::subimage(unsigned int level, int16_t xoffset, int16_t yoffset, uint16_t width, uint16_t height, format buf_format, void * buf)
	{
		::glTexSubImage2D(target, level, xoffset, yoffset, width, height, buf_format, GL_UNSIGNED_BYTE, buf);
		check_error();
	}

	void texture_2D::copy_image(unsigned int level, format int_format, int16_t x, int16_t y, uint16_t width, uint16_t height, uint16_t border)
	{
		::glCopyTexImage2D(target, level, int_format, x, y, width, height, border);
		check_error();
	}

	void texture_2D::copy_subimage(unsigned int level, int16_t xoffset, int16_t yoffset, int16_t x, int16_t y, uint16_t width, uint16_t height)
	{
		::glCopyTexSubImage2D(target, level, xoffset, yoffset, x, y, width, height);
		check_error();
	}

	// Buffer

	buffer::buffer(buffer_type target) :
		Target(target)
	{
		BOOST_ASSERT(initialized);
		::glGenBuffers(1, &Name);
		check_error();
	}

	buffer::~buffer()
	{
		::glDeleteBuffers(1, &Name);
	}

	void buffer::bind() const
	{
		::glBindBuffer(Target, Name);
		check_error();
	}

	void buffer::data(size_t size, const void * buf, usage_mode usage)
	{
		::glBufferData(Target, size, buf, usage);
		check_error();
	}

	void buffer::subdata(size_t offset, size_t size, const void * buf)
	{
		::glBufferSubData(Target, offset, size, buf);
		check_error();
	}

	// Shader

	shader::shader(shader_type type)
	{
		BOOST_ASSERT(initialized);
		Name = ::glCreateShader(type);
		check_error();
	}

	shader::~shader()
	{
		::glDeleteShader(Name);
	}

	int shader::get(unsigned int key) const
	{
		int value;
		::glGetShaderiv(Name, key, &value);
		check_error();
		return value;
	}

	void shader::source(size_t count, const char * const * strings, const int32_t * lengths)
	{
		const char ** gl_strings = const_cast<const char **> (strings);

		::glShaderSource(Name, count, gl_strings, lengths);
		check_error();
	}

	COMMON_CHECK_RETURN(bool) shader::compile()
	{
		::glCompileShader(Name);
		check_error();
		return get(GL_OBJECT_COMPILE_STATUS_ARB);
	}

	std::string shader::info_log() const
	{
		std::string log;

		int len = get(GL_OBJECT_INFO_LOG_LENGTH_ARB);
		if (len > 0) {
			boost::scoped_array<char> buf(new char[len + 1]);
			::glGetShaderInfoLog(Name, len + 1, 0, buf.get());
			log = buf.get();
		}

		return log;
	}

	// Program

	program::program()
	{
		BOOST_ASSERT(initialized);
		Name = ::glCreateProgram();
		check_error();
	}

	program::~program()
	{
		::glDeleteProgram(Name);
	}

	int program::get(unsigned int key) const
	{
		int value;
		::glGetProgramiv(Name, key, &value);
		check_error();
		return value;
	}

	void program::attach(const shader & s)
	{
		::glAttachShader(Name, s.Name);
		check_error();
	}

	void program::detach(const shader & s)
	{
		::glDetachShader(Name, s.Name);
		check_error();
	}

	COMMON_CHECK_RETURN(bool) program::link()
	{
		::glLinkProgram(Name);
		check_error();
		return get(GL_OBJECT_LINK_STATUS_ARB);
	}

	COMMON_CHECK_RETURN(bool) program::validate()
	{
		::glValidateProgram(Name);
		check_error();
		return get(GL_OBJECT_VALIDATE_STATUS_ARB);
	}

	std::string program::info_log() const
	{
		std::string log;

		int len = get(GL_OBJECT_INFO_LOG_LENGTH_ARB);
		if (len > 0) {
			boost::scoped_array<char> buf(new char[len + 1]);
			::glGetProgramInfoLog(Name, len + 1, 0, buf.get());
			log = buf.get();
		}

		return log;
	}

	void program::attrib(const char * symbol, const gl::attrib & a) const
	{
		::glBindAttribLocation(a.Index, Name, symbol);
		check_error();
	}

	gl::attrib program::attrib(const char * symbol) const
	{
		int index = ::glGetAttribLocation(Name, symbol);
		check_error();
		return gl::attrib(index);
	}

	gl::uniform program::uniform(const char * symbol) const
	{
		int location = ::glGetUniformLocation(Name, symbol);
		check_error();
		return gl::uniform(location);
	}

	void program::use() const
	{
		::glUseProgram(Name);
		check_error();
	}

	// Attribute

	void attrib::enable() const
	{
		if (Index >= 0) {
			::glEnableVertexAttribArray(Index);
			check_error();
		}
	}

	void attrib::disable() const
	{
		if (Index >= 0) {
			::glDisableVertexAttribArray(Index);
			check_error();
		}
	}

	void attrib::pointer(size_t size, data_type type, bool normalize, size_t stride, size_t offset) const
	{
		if (Index >= 0) {
			void *gl_offset = reinterpret_cast<void *> (offset);

			::glVertexAttribPointer(Index, size, type, normalize, stride, gl_offset);
			check_error();
		}
	}

	// Uniform

	uniform::uniform(int location) :
		Location(location)
	{
	}

	void uniform::set(float x) const
	{
		::glUniform1f(Location, x);
		check_error();
	}

	void uniform::set(float x, float y) const
	{
		::glUniform2f(Location, x, y);
		check_error();
	}

	void uniform::set(float x, float y, float z) const
	{
		::glUniform3f(Location, x, y, z);
		check_error();
	}

	void uniform::set(float x, float y, float z, float w) const
	{
		::glUniform4f(Location, x, y, z, w);
		check_error();
	}

	void uniform::set(int32_t x) const
	{
		::glUniform1i(Location, x);
		check_error();
	}

	void uniform::set(int32_t x, int32_t y) const
	{
		::glUniform2i(Location, x, y);
		check_error();
	}

	void uniform::set(int32_t x, int32_t y, int32_t z) const
	{
		::glUniform3i(Location, x, y, z);
		check_error();
	}

	void uniform::set(int32_t x, int32_t y, int32_t z, int32_t w) const
	{
		::glUniform4i(Location, x, y, z, w);
		check_error();
	}

	void uniform::set(size_t count, const float * f) const
	{
		::glUniform1fv(Location, count, f);
		check_error();
	}

	void uniform::set(size_t count, const vec<2,float> * v) const
	{
		::glUniform2fv(Location, count, v->data());
		check_error();
	}

	void uniform::set(size_t count, const vec<3,float> * v) const
	{
		::glUniform3fv(Location, count, v->data());
		check_error();
	}

	void uniform::set(size_t count, const vec<4,float> * v) const
	{
		::glUniform4fv(Location, count, v->data());
		check_error();
	}

	void uniform::set(size_t count, const int32_t * i) const
	{
		::glUniform1iv(Location, count, i);
		check_error();
	}

	void uniform::set(size_t count, const vec<2,int32_t> * v) const
	{
		::glUniform2iv(Location, count, v->data());
		check_error();
	}

	void uniform::set(size_t count, const vec<3,int32_t> * v) const
	{
		::glUniform3iv(Location, count, v->data());
		check_error();
	}

	void uniform::set(size_t count, const vec<4,int32_t> * v) const
	{
		::glUniform4iv(Location, count, v->data());
		check_error();
	}

	void uniform::set(size_t count, const mat<2,float> * m) const
	{
		::glUniformMatrix2fv(Location, count, GL_FALSE, m->data());
		check_error();
	}

	void uniform::set(size_t count, const mat<3,float> * m) const
	{
		::glUniformMatrix3fv(Location, count, GL_FALSE, m->data());
		check_error();
	}

	void uniform::set(size_t count, const mat<4,float> * m) const
	{
		::glUniformMatrix4fv(Location, count, GL_FALSE, m->data());
		check_error();
	}

	// Draw

	void draw_arrays(draw_mode mode, size_t index, size_t count)
	{
		::glDrawArrays(mode, index, count);
		check_error();
	}

	void draw_elements(draw_mode mode, data_type type, size_t index, size_t count)
	{
		void * gl_index = reinterpret_cast<void *> (index);

		::glDrawElements(mode, count, type, gl_index);
		check_error();
	}

	void multi_draw_arrays(draw_mode mode, const uint32_t * indices, const uint32_t * counts, size_t num)
	{
		int32_t * gl_indices = const_cast<int32_t *> (reinterpret_cast<const int32_t *> (indices));
		int32_t * gl_counts  = const_cast<int32_t *> (reinterpret_cast<const int32_t *> (counts));

		::glMultiDrawArrays(mode, gl_indices, gl_counts, num);
		check_error();
	}

	void multi_draw_elements(draw_mode mode, data_type type, const uint32_t * indices, const uint32_t * counts, size_t num)
	{
		const void ** gl_indices = const_cast<const void **> (reinterpret_cast<const void * const *> (indices));
		int32_t * gl_counts = const_cast<int32_t *> (reinterpret_cast<const int32_t *> (counts));

		::glMultiDrawElements(mode, gl_counts, type, gl_indices, num);
		check_error();
	}

}
