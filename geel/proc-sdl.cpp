/*
 * Copyright 2006  Timo Savola
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 */

#include "proc.hpp"
#include <SDL/SDL.h>

namespace gl {

	proc_type get_proc_address(const char * symbol)
	{
		union {
			void * address;
			proc_type proc;
		};

		address = SDL_GL_GetProcAddress(symbol);
		return proc;
	}

}
