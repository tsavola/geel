/*
 * Copyright 2006  Timo Savola
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 */

#include "context.hpp"

namespace gl {

	context_error::context_error(const char * message) :
		Message(message)
	{
	}

	context_error::~context_error()
		throw ()
	{
	}

	const char * context_error::what() const
		throw ()
	{
		return Message;
	}

}
