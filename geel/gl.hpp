/*
 * Copyright 2004-2006  Timo Savola
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 */

/**
 * \file geel/gl.hpp
 * OpenGL interface
 */

#ifndef GEEL_GL_HPP
#define GEEL_GL_HPP

#include <space/linear.hpp>
#include <space/angle.hpp>
#include <common/value.hpp>
#include <common/return.hpp>
#include <common/debug/backtrace.hpp>
#include <boost/utility.hpp>
#include <boost/cstdint.hpp>
#include <GL/gl.h>
#include <GL/glext.h>
#include <string>
#include <vector>
#include <exception>
#include <cstddef>

namespace gl {

	using boost::int8_t;
	using boost::int16_t;
	using boost::int32_t;
	using boost::uint8_t;
	using boost::uint16_t;
	using boost::uint32_t;

	using space::vec;
	using space::mat;
	using space::angle;

	// Data types

	enum data_type
	{
		int8_type               = GL_BYTE,
		int16_type              = GL_SHORT,
		int32_type              = GL_INT,
		uint8_type              = GL_UNSIGNED_BYTE,
		uint16_type             = GL_UNSIGNED_SHORT,
		uint32_type             = GL_UNSIGNED_INT,
		float_type              = GL_FLOAT,
		double_type             = GL_DOUBLE
	};

	template <typename T> struct type;

	template <> struct type<int8_t>   { static data_type value; };
	template <> struct type<int16_t>  { static data_type value; };
	template <> struct type<int32_t>  { static data_type value; };
	template <> struct type<uint8_t>  { static data_type value; };
	template <> struct type<uint16_t> { static data_type value; };
	template <> struct type<uint32_t> { static data_type value; };
	template <> struct type<float>    { static data_type value; };
	template <> struct type<double>   { static data_type value; };

	// Error

	enum error_code
	{
		invalid_enum            = GL_INVALID_ENUM,
		invalid_value           = GL_INVALID_VALUE,
		invalid_operation       = GL_INVALID_OPERATION,
		stack_overflow          = GL_STACK_OVERFLOW,
		stack_underflow         = GL_STACK_UNDERFLOW,
		out_of_memory           = GL_OUT_OF_MEMORY,
		table_too_large         = GL_TABLE_TOO_LARGE
	};

	struct extension_error : public std::exception
	{
		typedef std::vector<std::string> missing_type;

		virtual ~extension_error() throw ();
		virtual const char * what() const throw ();

		missing_type extensions;
		missing_type functions;
	};

	struct call_error : public std::exception
	{
		typedef common::backtrace_container backtrace_type;

		explicit call_error(error_code);
		virtual ~call_error() throw ();
		virtual const char * what() const throw ();

		const error_code code;
		backtrace_type backtrace;
	};

	// Initialization

	void init() throw (extension_error);

	// Flush

	void flush();

	// Viewport

	void viewport(int16_t x, int16_t y, uint16_t width, uint16_t height);

	// Scissor

	void scissor(int16_t x, int16_t y, uint16_t width, uint16_t height);
	void scissor(const vec<2,uint16_t> & size);
	void scissor(const vec<2,int16_t> & origin, const vec<2,uint16_t> & size);

	// Capabilities

	enum capability
	{
		cull_face               = GL_CULL_FACE,
		depth_test              = GL_DEPTH_TEST,
		stencil_test            = GL_STENCIL_TEST,
		alpha_test              = GL_ALPHA_TEST,
		blend                   = GL_BLEND,
		scissor_test            = GL_SCISSOR_TEST,
		texturing_1D            = GL_TEXTURE_1D,
		texturing_2D            = GL_TEXTURE_2D,
		texturing_3D            = GL_TEXTURE_3D,
		texturing_cube          = GL_TEXTURE_CUBE_MAP_ARB
	};

	void enable(capability);
	void disable(capability);

	// Clear

	enum
	{
		color_buffer_bit        = GL_COLOR_BUFFER_BIT,
		depth_buffer_bit        = GL_DEPTH_BUFFER_BIT
	};

	void clear_color(float r, float g, float b, float a);
	void clear_depth(double z);
	void clear(unsigned int bitmask);

	template <typename T> void clear_color(const vec<4,T> &);
	template <typename T> void clear_color(const vec<3,T> &, float);

	// Matrix

	enum matrix_stack
	{
		modelview               = GL_MODELVIEW0_ARB,
		projection              = GL_PROJECTION,
		texcoord                = GL_TEXTURE
	};

	void matrix_mode(matrix_stack);
	void push_matrix();
	void pop_matrix();

	void load_identity();
	void load_matrix(const mat<4,float> &);
	void load_matrix(const mat<4,double> &);
	void mult_matrix(const mat<4,float> &);
	void mult_matrix(const mat<4,double> &);

	// Projection

	void frustum(double left, double right, double bottom, double top, double near, double far);
	void ortho(double left, double right, double bottom, double top, double near, double far);

	// Transformation

	void rotate(float, float, float, float);
	void rotate(double, double, double, double);

	void translate(float, float, float);
	void translate(double, double, double);

	void scale(float, float, float);
	void scale(double, double, double);

	template <typename T> void rotate(const T & degrees, const vec<3,T> &);
	template <typename Unit, typename T> void rotate(const angle<Unit,T> &, const vec<3,T> &);
	template <typename Unit, typename T> void rotate(const angle<Unit,T> &, const T &, const T &, const T &);

	template <typename T> void translate(const vec<3,T> &);
	template <typename T> void scale(const vec<3,T> &);

	// Texture unit

	typedef common::value<0,32> texture_unit;

	void active_texture(texture_unit);
	void active_texcoord(texture_unit);

	// Texture

	enum filter_mode
	{
		nearest                 = GL_NEAREST,
		linear                  = GL_LINEAR
	};

	enum wrap_mode
	{
		repeat                  = GL_REPEAT,
		clamp                   = GL_CLAMP,
		clamp_to_border         = GL_CLAMP_TO_BORDER_ARB
	};

	enum format
	{
		red                     = GL_RED,
		green                   = GL_GREEN,
		blue                    = GL_BLUE,
		alpha                   = GL_ALPHA,
		rgb                     = GL_RGB,
		rgba                    = GL_RGBA,
		luminance               = GL_LUMINANCE,
		luminance_alpha         = GL_LUMINANCE_ALPHA,

		// Internal format only
		intensity               = GL_INTENSITY
	};

	struct texture : public boost::noncopyable
	{
		~texture();

		void bind() const;

		void min_filter(filter_mode);
		void mag_filter(filter_mode);

		void border_color(const vec<4,int32_t> & color);
		void border_color(const vec<4,float> & color);

		const capability target;

	protected:
		explicit texture(capability);

	private:
		unsigned int Name;
	};

	struct texture_1D : public texture
	{
		texture_1D();

		void wrap(wrap_mode);

		void image(unsigned int level, format internal, uint16_t w, uint16_t border, format buffer, void *);
		void subimage(unsigned int level, int16_t xoffset, uint16_t w, format buffer, void *);
		void copy_image(unsigned int level, format internal, int16_t x, int16_t y, uint16_t w, uint16_t border);
		void copy_subimage(unsigned int level, int16_t xoffset, int16_t x, int16_t y, uint16_t w);
	};

	struct texture_2D : public texture
	{
		texture_2D();

		void wrap_s(wrap_mode);
		void wrap_t(wrap_mode);

		void image(unsigned int level, format internal, uint16_t w, uint16_t h, uint16_t border, format buffer, void *);
		void subimage(unsigned int level, int16_t xoffset, int16_t yoffset, uint16_t w, uint16_t h, format buffer, void *);
		void copy_image(unsigned int level, format internal, int16_t x, int16_t y, uint16_t w, uint16_t h, uint16_t border);
		void copy_subimage(unsigned int level, int16_t xoffset, int16_t yoffset, int16_t x, int16_t y, uint16_t w, uint16_t h);
	};

	// Buffer

	enum buffer_type
	{
		array                   = GL_ARRAY_BUFFER_ARB,
		element_array           = GL_ELEMENT_ARRAY_BUFFER_ARB
	};

	enum usage_mode
	{
		stream_draw             = GL_STREAM_DRAW_ARB,
		static_draw             = GL_STATIC_DRAW_ARB,
		dynamic_draw            = GL_DYNAMIC_DRAW_ARB
	};

	struct buffer : public boost::noncopyable
	{
		explicit buffer(buffer_type);
		~buffer();

		void bind() const;

		void data(size_t size, const void *, usage_mode);
		void subdata(size_t offset, size_t size, const void *);

	protected:
		const buffer_type Target;

	private:
		unsigned int Name;
	};

	// Shader

	enum shader_type
	{
		vertex_shader           = GL_VERTEX_SHADER_ARB,
		fragment_shader         = GL_FRAGMENT_SHADER_ARB
	};

	struct shader : public boost::noncopyable
	{
		friend struct program;

		explicit shader(shader_type);
		~shader();

		void source(size_t count, const char * const * strings, const int32_t * lengths);
		COMMON_CHECK_RETURN(bool) compile();
		std::string info_log() const;

	private:
		int get(unsigned int) const;

		unsigned int Name;
	};

	// Program

	struct attrib;
	struct uniform;

	struct program : public boost::noncopyable
	{
		friend struct uniform;

		program();
		~program();

		void attach(const shader &);
		void detach(const shader &);

		COMMON_CHECK_RETURN(bool) link();
		COMMON_CHECK_RETURN(bool) validate();
		std::string info_log() const;

		void attrib(const char *, const gl::attrib &) const;
		gl::attrib attrib(const char *) const;
		gl::uniform uniform(const char *) const;

		void use() const;

	private:
		typedef unsigned int name_type;

		int get(unsigned int) const;

		name_type Name;
	};

	// Attribute

	struct attrib
	{
		friend struct program;

		attrib(int = -1);
		attrib(const attrib &);

		attrib & operator=(int);
		attrib & operator=(const attrib &);

		void enable() const;
		void disable() const;

		void pointer(size_t size, data_type, bool normalize, size_t stride, size_t offset) const;

	private:
		int Index;
	};

	// Uniform

	struct uniform
	{
		friend struct program;

		uniform();
		uniform(const uniform &);

		uniform & operator=(const uniform &);

		void set(float) const;
		void set(float, float) const;
		void set(float, float, float) const;
		void set(float, float, float, float) const;

		void set(int32_t) const;
		void set(int32_t, int32_t) const;
		void set(int32_t, int32_t, int32_t) const;
		void set(int32_t, int32_t, int32_t, int32_t) const;

		void set(size_t count, const float *) const;
		void set(size_t count, const int32_t *) const;

		void set(size_t count, const vec<2,float> *) const;
		void set(size_t count, const vec<3,float> *) const;
		void set(size_t count, const vec<4,float> *) const;

		void set(size_t count, const vec<2,int32_t> *) const;
		void set(size_t count, const vec<3,int32_t> *) const;
		void set(size_t count, const vec<4,int32_t> *) const;

		void set(size_t count, const mat<2,float> *) const;
		void set(size_t count, const mat<3,float> *) const;
		void set(size_t count, const mat<4,float> *) const;

		template <unsigned int N, typename T> void set(const vec<N,T> &) const;
		template <unsigned int N> void set(const mat<N,float> &) const;
		template <typename T> void set(const std::vector<T> &) const;

	private:
		explicit uniform(int);

		int Location;
	};

	// Draw

	enum draw_mode
	{
		triangles               = GL_TRIANGLES,
		triangle_strip          = GL_TRIANGLE_STRIP,
		triangle_fan            = GL_TRIANGLE_FAN,
		quads                   = GL_QUADS,
		quad_strip              = GL_QUAD_STRIP
	};

	void draw_arrays(draw_mode, size_t index, size_t count);
	void draw_elements(draw_mode, data_type, size_t index, size_t count);

	void multi_draw_arrays(draw_mode, const uint32_t * indices, const uint32_t * counts, size_t num);
	void multi_draw_elements(draw_mode, data_type, const uint32_t * indices, const uint32_t * counts, size_t num);

}

#include "gl.tpp"
#endif
