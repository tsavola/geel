/*
 * Copyright 2006  Timo Savola
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 */

#ifndef GEEL_CONTEXT_HPP
#define GEEL_CONTEXT_HPP

#include <exception>

namespace gl {

	struct context_error : public std::exception
	{
		explicit context_error(const char *);
		virtual ~context_error() throw ();
		virtual const char * what() const throw ();

	private:
		const char * Message;
	};

}

#endif
