/*
 * Copyright 2006  Timo Savola
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 */

#include "xembed.hpp"
#include <common/visibility.hpp>

namespace gl {

	COMMON_VISIBILITY_HIDDEN void set_xembed_info(Display * display, Window window, xembed_info flags)
		throw (context_error)
	{
		Atom atom = XInternAtom(display, "_XEMBED_INFO", False);
		if (atom == None)
			throw context_error("Failed to intern _XEMBED_INFO atom");

		unsigned long data[2] = {
			GEEL_XEMBED_VERSION,
			flags
		};

		unsigned char * x_data = reinterpret_cast<unsigned char *> (data);
		XChangeProperty(display, window, atom, atom, 32, PropModeReplace, x_data, 2);
	}

}
