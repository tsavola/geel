/*
 * Copyright 2006  Timo Savola
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 */

#ifndef GEEL_XEMBED_HPP
#define GEEL_XEMBED_HPP

#include "window.hpp"
#include "context.hpp"
#include <X11/Xlib.h>

#define GEEL_XEMBED_VERSION 0

namespace gl {

	enum xembed_info
	{
		xembed_mapped = 1 << 0
	};

	void set_xembed_info(Display *, Window, xembed_info) throw (context_error);

}

#endif
