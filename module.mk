# Copyright 2006  Timo Savola
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

GEEL_TARGET		= $(TARGET)
GEEL_TARGETDIR		= $(GEEL)/$(TARGETBASE)/$(GEEL_TARGET)

CPPFLAGS		+= -I$(GEEL)

TAGS_INCLUDES		+= $(GEEL)

geel_NAME		= geel
geel_TARGETDIR		= $(GEEL_TARGETDIR)

$(call export_library,geel)

geel_context_NAME	= geel-context
geel_context_TARGETDIR	= $(GEEL_TARGETDIR)

$(call export_library,geel_context)
